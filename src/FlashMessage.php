<?php

namespace Tetrapak07\FlashMessage;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;

class FlashMessage extends Controller
{
      
    public function initialize()
    {
        $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('flashmsg')) {

            $this->dataReturn = false;
        }   
    }
    
    public function onConstruct()
    {
       
    }

    public function message($type = 'error', $dataUrl = '', $mess = "Not found!", $redirect = false)
    {
        $data = false;
        if (!$this->dataReturn) {
            return $data;
        }
        $this->flash->$type($mess); 
        if ($redirect) {
         header("Location: $dataUrl");
         exit;
        }
        return true;

    }
    
}
